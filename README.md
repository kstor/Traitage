Traitage

Envoie un SMS d'insulte automatiquement a un numéro. Dictionnaire de 1600 insultes FR :)

Il faut configurer dans le fichier ~/.traitagerc une commande valable d’envoi de SMS, ici le binaire de Termux-api sous Android. Kdeconnect permet d'en faire autant sous GNU/Linux. Le numéro de téléphone est aussi à renseigner dans le script lui-même.

Très _très_ utiles pour la lutte active contre les mythiques, il vont retourner dans leurs montagnes loin des antennes GSM et 4G \o/

Une version CRONable est en projet, motivez moi par vos millions de téléchargements.

https://castorcorp.eu/kstor/Traitage - https://gitlab.com/kstor/Traitage
